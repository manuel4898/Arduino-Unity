#include <LiquidCrystal.h>

LiquidCrystal lcd(5, 4, 11, 10, 9, 8);
String incoming;
const int sw = 2;
const int x = A0;
const int y = A1;
long a, b, c;
long d;


int power = 0;
bool on = false;
void setup() {
  pinMode(sw, INPUT);
  digitalWrite(sw, HIGH);
  Serial.begin(9600);
  lcd.begin(16,2);
}

void loop() {
  power = digitalRead(7);

  if (power == HIGH)
    on = !on;
  if (on)
  {
  a = digitalRead(sw);
  b = analogRead(x);
  c = analogRead(y);
  d = a  * 100000000 + b * 10000 + c;
  Serial.println(d);
  }
  for (int i = 0; i < 16; i++)
  {
    lcd.setCursor(i,0);
    lcd.print(" ");
    lcd.setCursor(i,1);
    lcd.print(" ");
  }
  
  lcd.setCursor(0,1);
  lcd.print(a);
  
  lcd.setCursor(6,1);
  lcd.print(b);
  
  lcd.setCursor(12,1);
  lcd.print(c);
  
  delay(300);
  if (Serial.available() > 0) {
    incoming = Serial.read();
    lcd.setCursor(6,0);
    lcd.print(incoming);
  }
}
