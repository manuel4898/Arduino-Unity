using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;

public class Arduino : MonoBehaviour
{
    string[] puertos;
    SerialPort arduinoUno;
    public Dropdown lista_puertos;
    string puerto_seleccionado;
    bool conected;
    public GameObject cubo;
    public Text text;

    private void Awake()
    {
        lista_puertos.options.Clear();
        puertos = SerialPort.GetPortNames();

        foreach (string port in puertos)
        {
            lista_puertos.options.Add(new Dropdown.OptionData() { text = port });
        }
        DropdownItemSelected(lista_puertos);
        lista_puertos.onValueChanged.AddListener(delegate { DropdownItemSelected(lista_puertos); });
        Debug.Log(puertos);
    }
    public void Conectar()
    {
        if (!conected)
        {
            arduinoUno = new SerialPort(puerto_seleccionado, 9600, Parity.None, 8, StopBits.One);
            arduinoUno.Open();
            Debug.Log("Conectado");

            conected = true;
        }
    }
    public void Desconectar()
    {
        if (conected)
        {
            arduinoUno.Close();
            Debug.Log("Desconectado");

            conected = false;
        }
    }

    public void DropdownItemSelected(Dropdown lista) {
        int indice = lista.value;
        puerto_seleccionado = lista_puertos.options[indice].text;
        Debug.Log(puerto_seleccionado);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (cubo.transform.position.y >= 0.5)
        {
            cubo.transform.Translate(new Vector3(0, -1, 0) * 5.0f * Time.deltaTime);
        }

        if (conected)
        {
            string resultado = arduinoUno.ReadLine();
            long num = long.Parse(resultado);
            Debug.Log(num);

            long a_x = num % 10000;
            long a_z = (num % 100000000)/10000;
            long a_sw = num / 100000000;

            if (a_x > 550)
                cubo.transform.Translate(new Vector3(1, 0, 0) * 5.0f * Time.deltaTime);
            else if (a_x < 450)
                cubo.transform.Translate(new Vector3(-1, 0, 0) * 5.0f * Time.deltaTime);

            if (a_z > 550)
                cubo.transform.Translate(new Vector3(0, 0, 1) * 5.0f * Time.deltaTime);
            else if (a_z < 450)
                cubo.transform.Translate(new Vector3(0, 0, -1) * 5.0f * Time.deltaTime);

            if (a_sw == 0)
                cubo.transform.Translate(new Vector3(0, 1, 0) * 10.0f * Time.deltaTime);

        }

    }

    public void Send()
    {
        if (conected)
        {
            arduinoUno.WriteLine(text.text);
        }
    }
}
